# ReversAI

## Overview

This project is an intelligent non-human agent manifested in the form of a [Reversi](https://en.wikipedia.org/wiki/Reversi) player. The project itself is a part of a final assignment project for Intelligent System Course @ CSUI 2019.

Within this agent, we applied the MiniMax (Adversarial Search) Algorithm with Alpha-Beta Pruning.

The final product can be accessed through : https://reversai.netlify.com

## Downloads and Usages

### Requirements

* Terminal
* NodeJS
* Npm (Node Package Manager)
* Browser (Preferably Chrome)

### Downloads

* You can zip this project and extract it in your chosen directory. If you wish to walk this path, continue to the usage section.
* Else, make sure you have git installed in your environment, run this in your terminal

    ```bash
    $ git clone https://gitlab.com/reversai/reversi-ai.git
    ```

### Usage

* Assuming the requirements above is fulfilled, move into the project directory and follow these steps
    * Install dependencies
        ```bash
        $ npm install
        ```
    * Run the application in the development mode
        ```bash
        $ npm start
        ```
* Open [http://localhost:3000](http://localhost:3000) to view it in the browser. 
* The page will reload if you make edits.
* You will also see any lint errors in the console.

## Authors (Group: Nezza)

* [Dave Nathanael](https://gitlab.com/davenathanael)
* [Jonathan Christopher Jakub](https://gitlab.com/jonathanjojo)
* [Nandhika Prayoga](https://gitlab.com/Nan011)

## Acknowledgements

* Intelligent System CSUI 2019

## Others

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).