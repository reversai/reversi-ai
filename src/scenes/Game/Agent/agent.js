import heuristics from './heuristics';
import deepcopy from 'deepcopy';
import { setDisk } from '../utils';

class Agent {
    solve(state, level) {
        let depth = 2*(level) - 1;
        let best = new Move(Number.MIN_SAFE_INTEGER, undefined, undefined);
        let currentState = deepcopy(state);
        let toBeState = deepcopy(currentState);
        for (let row = 0; row < 8; row++) {
            for (let col = 0; col < 8; col++) {
                if (toBeState.suggestedTiles[row][col]) {
                    setDisk(toBeState, row, col);
                    let child = this.execute(deepcopy(toBeState), depth-1, false, Number.MIN_SAFE_INTEGER, Number.MAX_SAFE_INTEGER);
                    if (child.score >= best.score) {
                        best.score = child.score;
                        best.row = row;
                        best.col = col; 
                    }
                }
                toBeState = deepcopy(currentState);
            }
        }
        return best;
    }

    execute(state, depth, maximize, alpha, beta) {
        if (depth <= 0) {
            return new Move(heuristics(state, maximize), undefined, undefined);
        } else {
            state.isWhiteTurn = !maximize;
            let best = new Move(maximize? Number.MIN_SAFE_INTEGER : Number.MAX_SAFE_INTEGER, undefined, undefined);
            let currentState = deepcopy(state);
            let toBeState = deepcopy(state);
            let availableMove = false;
            for (let row = 0; row < 8; row++) {
                for (let col = 0; col < 8; col++) {
                    if (toBeState.suggestedTiles[row][col]) {
                        availableMove = true;
                        setDisk(toBeState, row, col);
                        let child = this.execute(deepcopy(toBeState), depth-1, !maximize, alpha, beta);
                        if (maximize) {
                            if (child.score >= best.score) {
                                best.score = child.score;
                                best.row = row;
                                best.col = col;
                            }
                            if (best.score >= alpha) {
                                alpha = best.score;
                            }
                            if (child.score >= beta) {
                                return best;
                            }
                        } else {
                            if (child.score <= best.score) {
                                best.score = child.score;
                                best.row = row;
                                best.col = col;
                            }
                            if (best.score <= beta) {
                                beta = best.score;
                            }
                            if (child.score <= alpha) {
                                return best;
                            }
                        }
                    }
                    toBeState = deepcopy(currentState);
                }
            }
            if (!availableMove) {
                return this.execute(deepcopy(toBeState), 0, maximize, alpha, beta);
            }
            return best; 
        }
    }
}

class Move {
    constructor(score, row, col) {
        this.score = score;
        this.row = row;
        this.col = col;
    }
}

export { Agent, Move }