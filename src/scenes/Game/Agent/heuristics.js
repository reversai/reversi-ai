const heuristics = (data, isBlack) => {    
    const parity = () => {
        const { blackDiskTotal, whiteDiskTotal } = data;
        const remainingDisks = 64 - (blackDiskTotal + whiteDiskTotal);
        return remainingDisks % 2 === 0 ? -1 : 1;
    };
    const discDifference = () => {
        const { blackDiskTotal, whiteDiskTotal } = data;
        return isBlack ? 
            (blackDiskTotal - whiteDiskTotal) / (blackDiskTotal + whiteDiskTotal) :
            (whiteDiskTotal - blackDiskTotal) / (blackDiskTotal + whiteDiskTotal);
    };
    const mobility = () => {
        return weight(data.suggestedTiles);
    };
    const weight = (disks = data.diskColors) => {
        const weights = [
            [500    , -100  , 200   , 100   , 100    , 200   , -100  , 500 ],
            [-100   , -200  , -50   , -50   , -50    , -50   , -200  , -100],
            [100    , -50   , 100   , 0     , 0      , 100   , -50   , 100 ],
            [50     , -50   , 0     , 0     , 0      , 0     , -50   , 50  ],
            [50     , -50   , 0     , 0     , 0      , 0     , -50   , 50  ],
            [100    , -50   , 100   , 0     , 0      , 100   , -50   , 100 ],
            [-100   , -200  , -50   , -50   , -50    , -50   , -200  , -100],
            [500    , -100  , 200   , 100   , 100    , 200   , -100  , 500 ]
        ];

        if (disks[0][0] !== undefined) {
            weights[0][1] = 0;
            weights[0][2] = 0;
            weights[0][3] = 0;
            weights[1][0] = 0;
            weights[1][1] = 0;
            weights[1][2] = 0;
            weights[1][3] = 0;
            weights[2][0] = 0;
            weights[2][1] = 0;
            weights[2][2] = 0;
            weights[3][0] = 0;
            weights[3][1] = 0;
        }

        if (disks[0][7] !== undefined) {
            weights[0][4] = 0;
            weights[0][5] = 0;
            weights[0][6] = 0;
            weights[1][4] = 0;
            weights[1][5] = 0;
            weights[1][6] = 0;
            weights[1][7] = 0;
            weights[2][5] = 0;
            weights[2][6] = 0;
            weights[2][7] = 0;
            weights[3][6] = 0;
            weights[3][7] = 0;
        }

        if (disks[7][0] !== undefined) {
            weights[4][0] = 0;
            weights[4][1] = 0;
            weights[5][0] = 0;
            weights[5][1] = 0;
            weights[5][2] = 0;
            weights[6][0] = 0;
            weights[6][1] = 0;
            weights[6][2] = 0;
            weights[6][3] = 0;
            weights[7][1] = 0;
            weights[7][2] = 0;
            weights[7][3] = 0;
        }

        if (disks[7][7] !== undefined) {
            weights[4][6] = 0;
            weights[4][7] = 0;
            weights[5][5] = 0;
            weights[5][6] = 0;
            weights[5][7] = 0;
            weights[6][4] = 0;
            weights[6][5] = 0;
            weights[6][6] = 0;
            weights[6][7] = 0;
            weights[7][4] = 0;
            weights[7][5] = 0;
            weights[7][6] = 0;
        }

        let result = 0;

        disks.forEach((innerArray, row) => {
            innerArray.forEach((e, col) => {
                result +=  e === isBlack ? weights[row][col] : 0;
            });
        });
        
        return result;
    };

    let parityMultiplier, discDifferenceMultiplier, mobilityMultiplier, weightMultiplier;
    const { blackDiskTotal, whiteDiskTotal } = data;
    const remainingDisks = 64 - (blackDiskTotal + whiteDiskTotal);
    if (remainingDisks <= 6) {
        // endgame
        parityMultiplier = 500;
        discDifferenceMultiplier = 500;
        mobilityMultiplier = 10;
        weightMultiplier = 10;
    } else if (remainingDisks <= 44) {
        // midgame
        parityMultiplier = 100;
        discDifferenceMultiplier = 20;
        mobilityMultiplier = 5;
        weightMultiplier = 10;
    } else {
        // earlygame
        parityMultiplier = 1;
        discDifferenceMultiplier = 1;
        mobilityMultiplier = 5;
        weightMultiplier = 20;
    }

    return parityMultiplier * parity() +
            discDifferenceMultiplier * discDifference() +
            mobilityMultiplier * mobility() +
            weightMultiplier * weight();
}

export default heuristics;