const initialDiskColors = [
    [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined],
    [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined],
    [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined],
    [undefined, undefined, undefined, true, false, undefined, undefined, undefined],
    [undefined, undefined, undefined, false, true, undefined, undefined, undefined],
    [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined],
    [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined],
    [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined]
]

const initialSuggestedTiles = [
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, true, false, false, false],
    [false, false, false, false, false, true, false, false],
    [false, false, true, false, false, false, false, false],
    [false, false, false, true, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false]
]

const initialFrontiers = [
    [2,2], [2,3], [2,4], [2,5],
    [3,2], 				 [3,5],
    [4,2],				 [4,5],
    [5,2], [5,3], [5,4], [5,5]
]

export {
    initialDiskColors, initialSuggestedTiles, initialFrontiers
}