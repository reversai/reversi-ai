const flipDiskAround = (data, row, col, manipulate) => {
    let diskIsFlipped = false;
    if (data.diskColors[row][col] === undefined) {
        const right = shouldDiskFlip(data, row, col + 1, 0, 1, manipulate);
        const left = shouldDiskFlip(data, row, col - 1, 0, -1, manipulate);
        const top = shouldDiskFlip(data, row + 1, col, 1, 0, manipulate);
        const bot = shouldDiskFlip(data, row - 1, col, -1, 0, manipulate);
        const q2Q4 = shouldDiskFlip(data, row - 1, col - 1, -1, -1, manipulate);
        const q4Q2 = shouldDiskFlip(data, row + 1, col + 1, +1, +1, manipulate);
        const q1Q3 = shouldDiskFlip(data, row - 1, col + 1, -1, +1, manipulate);
        const q3Q1 = shouldDiskFlip(data, row + 1, col - 1, +1, -1, manipulate);
    
        diskIsFlipped = right || left || top || bot || q2Q4 || q4Q2 || q1Q3 || q3Q1;
    
        if (diskIsFlipped && manipulate) {
            data.diskColors[row][col] = data.isWhiteDiskTurn;
            if (data.isWhiteDiskTurn) {
                data.whiteDiskTotal += 1;
            } else {
                data.blackDiskTotal += 1;
            }
        }    
    }
    return diskIsFlipped;
}

const setDisk = (data, row, col) => {
    
    flipDiskAround(data, row, col, true);

    deleteFromFrontiers(data, row, col);

    addToFrontiers(data, row, col+1);
    addToFrontiers(data, row, col-1);
    addToFrontiers(data, row+1, col+1);
    addToFrontiers(data, row+1, col);
    addToFrontiers(data, row+1, col-1);
    addToFrontiers(data, row-1, col+1);
    addToFrontiers(data, row-1, col);
    addToFrontiers(data, row-1, col-1);

    data.suggestedTiles = Array(data.diskColors.length).fill().map(() => Array(data.diskColors.length).fill(false));
   
    data.isWhiteDiskTurn = !data.isWhiteDiskTurn;

    let ableToMove = false;
    for(let element of data.frontiers) {
        let curRow = element[0];
        let curCol = element[1];
        let moveAvailable = flipDiskAround(data, curRow, curCol, false);
        if (moveAvailable) {
            ableToMove = true;
        }
        data.suggestedTiles[curRow][curCol] = moveAvailable;
    }

    return ableToMove;
}

const shouldDiskFlip = (data, row, col, rowInc, colInc, manipulate) => {
    let mustFlipDisk = false;
    let nextRow = row + rowInc;
    let nextCol = col + colInc;
    if (
        nextRow >= 0 && nextRow < data.diskColors.length &&
        nextCol >= 0 && nextCol < data.diskColors.length &&
        data.diskColors[row][col] === !data.isWhiteDiskTurn) {

        if (data.diskColors[nextRow][nextCol] === data.isWhiteDiskTurn) {
            mustFlipDisk = true;
        }

        if (!mustFlipDisk) {
            mustFlipDisk = shouldDiskFlip(data, nextRow, nextCol, rowInc, colInc, manipulate);
        }

        if (mustFlipDisk && manipulate) {
            data.diskColors[row][col] = data.isWhiteDiskTurn;

            if (data.isWhiteDiskTurn) {
                data.whiteDiskTotal += 1;
                data.blackDiskTotal -= 1;
            } else {
                data.whiteDiskTotal -= 1;
                data.blackDiskTotal += 1;
            }
        }
    }
    return mustFlipDisk;
}

const addToFrontiers = (data, row, col) => {
    if (
        row >= 0 && row < data.diskColors.length &&
        col >= 0 && col < data.diskColors.length &&
        inFrontiers(data, row, col) === -1
    ) {
        data.frontiers.push([row, col]);
    }
}

const inFrontiers = (data, row, col) => {
    for(let i = 0; i < data.frontiers.length; i++) {
        if (data.frontiers[i][0] === row && data.frontiers[i][1] === col) {
            return i;
        }
    }
    return -1;
}

const deleteFromFrontiers = (data, row, col) => {
    let i = inFrontiers(data, row, col);
    if (i !== -1) {
        data.frontiers.splice(i, 1);
    }
}

export {flipDiskAround, setDisk, shouldDiskFlip}