import React, { Component } from 'react';
import deepcopy from 'deepcopy';

import Button from '../../components/Button/Button';
import Dropdown from '../../components/Dropdown/Dropdown';
import Board from '../../components/Board/Board';
import Disk from '../../components/Disk/Disk';
import Dashboard from '../../components/Dashboard/index';
import Turn from '../../components/Turn/index';
import '../../support/styles/main.scss';
import './game.css';

import {flipDiskAround} from './utils';
import {initialDiskColors, initialSuggestedTiles, initialFrontiers} from './startingBoard';
import { Agent } from './Agent/agent';


class Game extends Component {
	constructor(props) {
		super(props);
		const diskColors = deepcopy(initialDiskColors);
		const suggestedTiles = deepcopy(initialSuggestedTiles);
		this.frontiers = deepcopy(initialFrontiers);

		this.levelList = Array(3).fill().map((_, i) => (
			{
				name: `level ${i+1}`, 
				do: () => this.changeLevel(i+1)
		 	}
		));

		this.modeList = [
			{
				name: 'single player',
				do: () => this.changeMode(true),
			},
			{
				name: 'multiplayer',
				do: () => this.changeMode(false),
			}
		];

		this.state = {
			singlePlayer: true,
			diskColors: diskColors,
			suggestedTiles: suggestedTiles,
			whiteDiskTotal: 2,
			blackDiskTotal: 2,
			isWhiteDiskTurn: true,
			playerDiskColor: true,
			dashboardAppear: true,
			aiDashboard: {
				aiLevel: {
					level: 1,
					maxLevel: this.levelList.length,
				},
				winPercentage: 50,
				executionTime: 0,
				fitness: 0,
			}
		}

		this.diskColorsHistory = [];
		this.whiteDiskTotalHistory = [];
		this.blackDiskTotalHistory = [];
		this.frontiersHistory = [];

		this.setDisk = this.setDisk.bind(this);
		this.changeLevel = this.changeLevel.bind(this);
		this.changeDashboardState = this.changeDashboardState.bind(this);
		this.undo = this.undo.bind(this);
		this.restart = this.restart.bind(this);
		this.changeMode = this.changeMode.bind(this);
		this.agent = new Agent();
	}

	changeLevel(level) {
		let aiDashboard = this.state.aiDashboard;
		aiDashboard.aiLevel.level = level;
		this.setState({
			aiDashboard,
		})
	}

	changeMode(singlePlayer) {
		this.setState({
			singlePlayer,
			dashboardAppear: singlePlayer? true: false,
		}, () => {
			this.playerMove();
		});
	}
	
	restart() {
		this.setState({
			diskColors: deepcopy(initialDiskColors),
			whiteDiskTotal: 2,
			blackDiskTotal: 2,
			isWhiteDiskTurn: true,
			suggestedTiles: deepcopy(initialSuggestedTiles)
		});

		this.whiteDiskTotalHistory = [];
		this.blackDiskTotalHistory = [];
		this.diskColorsHistory = [];
		this.frontiersHistory = [];
		this.frontiers = deepcopy(initialFrontiers);
	}

	nextMove() {
		this.setState({
			isWhiteDiskTurn: !this.state.isWhiteDiskTurn
		}, () => {
			let ableToMove = false;
			let nextSuggestions = Array(this.state.diskColors.length).fill().map(() => Array(this.state.diskColors.length).fill(false));
			for(let element of this.frontiers) {
				let row = element[0];
				let col = element[1];
				nextSuggestions[row][col] = this.isSuggestable(row, col);
				if (nextSuggestions[row][col]) {
					ableToMove = true;
				}
			}

			this.setState({
				suggestedTiles: nextSuggestions
			}, () => {
				if (!this.isEndgame()) {
					setTimeout(() => {
						if (!ableToMove) {
							this.diskColorsHistory.push(deepcopy(this.state.diskColors));
							this.whiteDiskTotalHistory.push(this.state.whiteDiskTotal);
							this.blackDiskTotalHistory.push(this.state.blackDiskTotal);
							this.frontiersHistory.push(deepcopy(this.frontiersHistory[this.frontiersHistory.length - 1]));
							this.nextMove();
						} else {
							this.playerMove();
						}
					}, 500);
				}
			});
		});

	}

	playerMove() {
		if (this.state.singlePlayer && !this.state.isWhiteDiskTurn) {
			// AI turn
			let beforeTime = performance.now();
			let data = deepcopy(this.state);
			data['frontiers'] = deepcopy(this.frontiers);

			let nextMove = this.agent.solve(data, this.state.aiDashboard.aiLevel.level);
			let afterTime = performance.now() - beforeTime;
			let aiDashboard = this.state.aiDashboard;
			aiDashboard.executionTime = (afterTime / 1000).toFixed(2);
			aiDashboard.fitness = nextMove.score.toFixed(2);
			aiDashboard.winPercentage = (Math.random() * 100).toFixed(2);

			this.setState({
				aiDashboard,
			}, () => {
				this.setDisk(nextMove.row, nextMove.col, true);
			});	
		} else {
			// User turn, triggered by clicked event.
		}
	}

	isEndgame() {
		if (this.state.whiteDiskTotal + this.state.blackDiskTotal >= Math.pow(this.state.diskColors.length, 2) ||
			this.state.whiteDiskTotal === 0 ||
			this.state.blackDiskTotal === 0) {
			
			if (this.state.blackDiskTotal === this.state.whiteDiskTotal) {
				this.props.sendPopupMessage("draw");
			} else if (this.state.blackDiskTotal >= this.state.whiteDiskTotal) {
				this.props.sendPopupMessage(this.state.singlePlayer? "ai win": "friend win");
			} else {
				this.props.sendPopupMessage("you win");
			}
			return true;
		}
		return false;
	}

	isSuggestable(row, col) {
		if (row >= 0 && row < this.state.diskColors.length &&
			col >= 0 && col < this.state.diskColors.length &&
			this.state.diskColors[row][col] === undefined &&
			this.setDisk(row, col, false)
			) {
				return true;
			}
		return false;
	}

	setDisk(row, col, manipulate) {
		if (row >= 0 && row < this.state.diskColors.length &&
			col >= 0 && col < this.state.diskColors.length &&
			this.state.diskColors[row][col] === undefined) {
			let currentDiskColors = deepcopy(this.state.diskColors);  

			let data = deepcopy(this.state);

			let diskIsFlipped = flipDiskAround(data, row, col, manipulate);
			
			if (diskIsFlipped && manipulate) {
				this.diskColorsHistory.push(currentDiskColors);
				this.whiteDiskTotalHistory.push(this.state.whiteDiskTotal);
				this.blackDiskTotalHistory.push(this.state.blackDiskTotal);
				this.frontiersHistory.push(deepcopy(this.frontiers));
				
				this.setState(data, () => {
					this.deleteFromFrontiers(this.inFrontiers(row, col));
	
					let allFrontiers = [
						[row - 1, col - 1], [row - 1, col], [row - 1, col + 1],
						[row, col - 1], 					[row, col + 1],
						[row + 1, col - 1], [row + 1, col], [row + 1, col + 1]
					];
	
					for (let element of allFrontiers) {
						let row = element[0];
						let col = element[1];
						if (row >= 0 && row < this.state.diskColors.length &&
							col >= 0 && col < this.state.diskColors.length &&
							data.diskColors[row][col] === undefined) {
							if (this.inFrontiers(row, col) === -1) {
								this.frontiers.push(element);
							}
						}
					}

					this.nextMove();
				});
			}
			return diskIsFlipped;
		}
		return false;
	}

	inFrontiers(row, col) {
		for(let i = 0; i < this.frontiers.length; i++) {
			if (this.frontiers[i][0] === row && this.frontiers[i][1] === col) {
				return i;
			}
		}
		return -1;
	}

	deleteFromFrontiers(i) {
		if (i !== -1) {
			this.frontiers.splice(i, 1);
		}
	}

	
	undo() {
		if (this.diskColorsHistory.length === 0) {
			this.restart();
		} else {
			this.frontiers = deepcopy(this.frontiersHistory.pop());
			this.setState({
				diskColors: this.diskColorsHistory.pop(),
				whiteDiskTotal: this.whiteDiskTotalHistory.pop(),
				blackDiskTotal: this.blackDiskTotalHistory.pop(),
			}, function() {
				if (this.state.singlePlayer && this.state.isWhiteDiskTurn) {
					this.setState({
						isWhiteDiskTurn: !this.state.isWhiteDiskTurn,
					}, () => {
						this.undo();
					});
				} else {
					this.nextMove();
				}
			});
		}
	}

	changeDashboardState() {
		this.setState({
			dashboardAppear: !this.state.dashboardAppear,
		});
	}

	render() {
		let buttonResponsiveRules = [
			{
				maxWidth: 800,
				padding: '5px 14px',
				fontSize: '.7em',
			}
		];

		return (
			<div id='l-game'>
				<div id='l-game__player' className='l-flex--column-nowrap'>
					<div id='l-player__disk-info' className='l-flex--column-nowrap l-flex--center-center'>
						<div className='l-player__disk h-center'>
							<label>
								{this.state.singlePlayer? 'ai': 'friend'}
							</label>
							<div id='l-player__enemy-disk'>
								<span className='h-center'>
									{this.state.blackDiskTotal}
								</span>
								<Disk isWhite={!this.state.playerDiskColor} size={23}/>
							</div>
						</div>
						<div className='l-player__disk h-center'>
							<label>
								you
							</label>
							<div id='l-player__your-disk' >
								<span className='h-center'>
									{this.state.whiteDiskTotal}
								</span>
								<Disk isWhite={this.state.playerDiskColor} size={23}/>
							</div>
						</div>
					</div>
					<div id='l-player__turn'>
						<Turn aiTurn={!this.state.isWhiteDiskTurn} secondPlayerName={this.state.singlePlayer? 'AI': 'FRIEND'} />
					</div>
					<div id='l-player__tools'>
						<div className='l-player__tools--row l-flex--row-nowrap'>
							<Button
								className='l-player__button' 
								name={'restart'} 
								do={() => this.restart()} 
								responsiveRules={buttonResponsiveRules}
							/>
							<Button 
								className='l-player__button' 
								name={'undo'} 
								do={() => this.undo()}
								responsiveRules={buttonResponsiveRules}
							/>
						</div>
						<div className='l-player__tools--row l-flex--row-nowrap'>
							<Dropdown 
								display={true} 
								className='l-player__button' 
								name={'level'} 
								list={this.levelList}
								responsiveRules={buttonResponsiveRules}
							/>
							<Dropdown 
								display={true} 
								className='l-player__button' 
								name={'mode'} 
								list={this.modeList}
								responsiveRules={buttonResponsiveRules}
							/>
						</div>
					</div>
				</div>
				<div id='l-game__board' class='h-center'>
					<Board 
						numberOfRowTile={this.state.diskColors.length} 
						diskColors={this.state.diskColors} 
						setDisk={(row, col) => this.setDisk(row, col, true)}
						suggestedTiles={this.state.suggestedTiles}
						canPlayerClick={this.state.isWhiteDiskTurn || !this.state.singlePlayer}
					/>
				</div>
				<div id='l-game__ai-player' className='l-flex--column-nowrap'>
					<Button 
						name="info" 
						do={() => this.changeDashboardState()}
						responsiveRules={buttonResponsiveRules}
					/>
					<div id='c-game__ai-player' className='h-animation-config' data-dashboard-appear={this.state.dashboardAppear}>
						<Dashboard 
							aiLevel={this.state.aiDashboard.aiLevel}
							winPercentage={this.state.aiDashboard.winPercentage}
							executionTime={this.state.aiDashboard.executionTime}
							fitness={this.state.aiDashboard.fitness}
						/>
					</div>
				</div>
			</div>
		);
	}
}

export default Game;
