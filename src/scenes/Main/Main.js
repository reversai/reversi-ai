import React, { Component } from 'react';

import Game from '../Game/Game';
import '../../support/styles/main.scss';
import './main.css';


class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showPopup: false,
			popupMessage: '',
		}

		this.showMessage = this.showMessage.bind(this);
	}

	showMessage(message) {
		this.setState({
			showPopup: true,
			popupMessage: message,
		}, () => {
			setTimeout(() => {
				if (this.state.showPopup) {
					this.setState({
						showPopup: false,
						popupMessage: '',
					})
				}
			}, 3000);
			
		});
	}

	render() {
		return (
			<section id='l-section' className='l-flex--row-nowrap l-flex--center-center'>
				<h1 id='l-section__title' class='h-font--small'>
					ReversAI
				</h1>
				<div 
					id='l-section__wall' 
					className='h-animation-config' 
					data-display={this.state.showPopup}
					onClick={() => {
						this.setState({
							showPopup: false,
							popupMessage: '',
						})
					}}
				>
				</div>
				<div 
					id='l-section__popup' 
					className='l-flex--column-nowrap l-flex--center-center h-animation-config' 
					data-display={this.state.showPopup}
					onClick={() => {
						this.setState({
							showPopup: false,
							popupMessage: '',
						})
					}}
				>
					<div id='c-popup__message' className='l-flex--column-nowrap l-flex--center-center h-font--small'>
						{this.state.popupMessage}
					</div>
				</div>
				<div id='l-section__footer'>
					&copy; copyright 2019. all rights reserved.
					<div id='l-devs' className='l-flex--row-nowrap l-flex--center-center'>
						<div id='c-devs__name'>
							dave nathanael - jonathan christopher j. - nandhika prayoga
						</div>
					</div>
				</div>
				<Game sendPopupMessage={(message) => this.showMessage(message)}/>
			</section>
		);
	}
}

export default Main;
