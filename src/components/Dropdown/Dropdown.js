import React, {Component} from 'react';

import Button from '../Button/Button';
import './dropdown.css';

class Dropdown extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listShowed: 'hide',
            style: null,
        }
        
        this.animateList = this.animateList.bind(this);
        this.listOnClick = this.listOnClick.bind(this);
    }

    animateList() {
        const listShowed = this.state.listShowed === 'appear' ? 'hide' : 'appear';

        this.setState({
            listShowed
        });
    }

    listOnClick(index) {
        this.props.list[index].do();
        this.animateList();
    }

    createList() {
        let list = []
        for(let index = 0; index < this.props.list.length; index++) {
            list.push(
                <button 
                    id='c-list__item' 
                    className='h-font--medium h-animation-config' 
                    onClick={() => this.listOnClick(index)}
                    style={this.state.style != null? {
                        padding: this.state.style.padding,
                        fontSize: this.state.style.fontSize, 
                    }: {}}
                >
                    {this.props.list[index].name}
                </button>
            );
        }
        return list;
    }

    componentWillMount() {
        let done = false;
        let index = 0;
        while (!done && this.props.responsiveRules != undefined && this.props.responsiveRules.length > index) {
            let rule = this.props.responsiveRules[index++];
            if (window.innerWidth < rule.maxWidth) {

                this.setState({
                    style: {
                        fontSize: rule.fontSize,
                        padding: rule.padding,
                    }
                });
                done = true;
            }
        }
    }

    render() {
        return (
            <div id='l-dropdown' className={`${this.props.className} l-flex--column-nowrap l-flex--center-center`} data-display={this.props.display} data-state={this.state.state}>
                <Button 
                    name={this.props.name} 
                    do={() => this.animateList()}
                    responsiveRules={this.props.responsiveRules}
                />
                <div id='l-dropdown__list' className='l-flex--column-nowrap l-flex--center-center' data-state={this.state.listShowed} ref={this.list}>
                    {this.createList()}
                </div>
            </div>
        );
    }
}

export default Dropdown;