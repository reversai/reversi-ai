import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ProgressBar from '../ProgressBar';
import './dashboard.css';

class Dashboard extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            aiLevel: this.props.aiLevel,
            winPercentage: this.props.winPercentage,
            executionTime: this.props.executionTime,
            fitness: this.props.fitness,
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            this.setState({
                aiLevel: nextProps.aiLevel,
                winPercentage: nextProps.winPercentage,
                executionTime: nextProps.executionTime,
                fitness: nextProps.fitness,
            });
        }
    }

    render() {
        const sFitness = this.state.fitness;
        const fitness = (Math.abs(sFitness) >= 1000) ? `${Math.trunc(sFitness / 1000)}.${Math.trunc(Math.trunc(sFitness % 1000) / 10)}K` : sFitness;
        return(
            <div>
                <div id="l-bars">
                    <div id="c-dashboardtitle">AI</div>
                    <div id="l-levelbar__llabel"><p>level</p></div>
                    <div id="l-levelbar__bar">
                        <ProgressBar fraction={{filled: this.state.aiLevel.level, total: this.state.aiLevel.maxLevel}} />
                    </div>
                    <div id="l-levelbar__rlabel"><p>{`${this.state.aiLevel.level}/${this.state.aiLevel.maxLevel}`}</p></div>
                </div>
                <div id="l-stats">
                    <p id="c-lastexec">last execution.</p>
                    <div id="l-numbers">
                        <div className="l-statwrapper">
                            <div className="c-statdisplay">
                                <div className='l-flex--row-nowrap l-flex--center-center'>
                                    <span>{this.state.executionTime}</span>
                                    <span>s</span>
                                </div>
                                </div>
                            <div className="c-statlabel">time</div>
                        </div>
                        <div className="l-statwrapper">
                            <div className="c-statdisplay">{fitness}</div>
                            <div className="c-statlabel">fitness</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Dashboard.propTypes = {
    aiLevel: PropTypes.shape({
        level: PropTypes.number,
        maxLevel: PropTypes.number,
    }),
    winPercentage: PropTypes.number,
    executionTime: PropTypes.number,
    fitness: PropTypes.number,
};

export default Dashboard;