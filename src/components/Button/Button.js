import React, {Component} from 'react';
import './button.css';

class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isClicked: 'idle',
            style: null,
        };
        
        this.clicked = this.clicked.bind(this);
    }

    clicked() {
        this.props.do();
        this.setState({isClicked: 'clicked'});
        setTimeout(() => {
            this.setState({isClicked: 'idle'});
        }, 1000);
    }

    componentWillMount() {
        let done = false;
        let index = 0;
        while (!done && this.props.responsiveRules != undefined && this.props.responsiveRules.length > index) {
            let rule = this.props.responsiveRules[index++];
            if (window.innerWidth < rule.maxWidth) {

                this.setState({
                    style: {
                        fontSize: rule.fontSize,
                        padding: rule.padding,
                    }
                });
                done = true;
            }
        }
    }

    render() {
        return (
            <button 
                id='l-button' 
                className={`${this.props.className}`} 
                data-state={this.state.state} 
                onClick={() => this.clicked()}
                style={this.state.style != null? {padding: this.state.style.padding}: {}}
            >
                <div id='c-button__border' className='h-animation-config' data-state={this.state.isClicked}>
                </div>
                <div 
                    id='c-button__content' 
                    className='h-font--medium h-animation-config' 
                    data-state={this.state.isClicked}
                    style={this.state.style != null? {fontSize: this.state.style.fontSize}: {}}
                >
                    {`${this.props.name}.`}
                </div>
            </button>
        );
    }
}

export default Button;