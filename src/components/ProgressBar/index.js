import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './progressbar.css';

class FixedSectionBar extends Component {
    constructor(props) {
        super(props);
        const percentages = Array(this.props.total).fill().map((e, i) => i < this.props.filled ? 100 : 0);

        this.state = {
            filled: this.props.filled,
            total: this.props.total,
            percentages
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            const percentages = Array(nextProps.total).fill().map((e, i) => i < nextProps.filled ? 100 : 0);

            this.setState({
                filled: nextProps.filled,
                total: nextProps.total,
                percentages
            });
        }
    }

    render() {    
        return (
            <div id="c-fractionbar__wrapper">
                {this.state.percentages.map((percentage, i) => 
                    <PercentageBar percentage={percentage} key={i} style={{ width: `${100 / this.state.total - 1}%`, margin: '.1em' }} />
                )}
            </div>
        );
    }
}

class PercentageBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            percentageStyle: {
                width: `${this.props.percentage}%`
            },
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            this.setState({
                percentageStyle: {
                    width: `${nextProps.percentage}%`,
                }
            });
        }
    }

    render() {
        return(
            <div id="c-percentagebar__wrapper" style={this.props.style}>
                <div id="c-percentagebar__filler" className='h-animation-config' style={this.state.percentageStyle} />
            </div>
        );
    }   
}

class ProgressBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            percentage: this.props.percentage,
            fraction: this.props.fraction,
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            this.setState({
                percentage: nextProps.percentage,
                fraction: nextProps.fraction,
            });
        }
    }

    display() {
        if (this.state.percentage === undefined)
            return <FixedSectionBar filled={this.state.fraction.filled} total={this.state.fraction.total} />;
        else
            return <PercentageBar percentage={this.state.percentage} />;
    }

    render() {
        return (this.display());
    }
};

ProgressBar.propTypes = {
    fraction: PropTypes.shape({
        filled: PropTypes.number.isRequired,
        total: PropTypes.number.isRequired,
    }),
    percentage: PropTypes.number,
};

export default ProgressBar;