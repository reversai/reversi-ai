import React, {Component} from 'react';
import './disk.css';


class Disk extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isWhite: this.props.isWhite
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            this.setState({
                isWhite: nextProps.isWhite,
            });
        }
    }

    render() {
        return (
            <div 
                id="disk" 
                className="h-animation-flip"
                data-color={this.state.isWhite}
                style={{width:this.props.size, height:this.props.size}}
                >
            </div>
        )
    }
}

export default Disk;