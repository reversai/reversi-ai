import React, {Component} from 'react';
import './board.css';
import Tile from "./Tile/Tile";

class Board extends Component {
    constructor(props) {
        super(props);
        this.state = {
            diskColors: this.props.diskColors,
            suggestedTiles: this.props.suggestedTiles
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            this.setState({
                diskColors: nextProps.diskColors,
                suggestedTiles: nextProps.suggestedTiles
            });
        }
    }

    flip(row, col) {
        if (this.props.canPlayerClick) {
            this.props.setDisk(row, col);
        }
    }

    display() {
        let displayTile = [];
        for(let row=-1; row < this.props.numberOfRowTile+1; row++) {
            let rowTile = [];
            if (row === -1 || row === this.props.numberOfRowTile) {
                for(let col=-1; col < this.props.numberOfRowTile+1; col++) {
                    if (col === -1 || col === this.props.numberOfRowTile) {
                        rowTile.push(
                            <div id="corner"></div>
                        );
                    } else {
                        let extension;
                        if (col === 0) {
                            extension = "left";
                        } else if (col === this.props.numberOfRowTile-1) {
                            extension = "right";
                        } else {
                            extension = "none";
                        }
                        rowTile.push(
                            <div 
                                id="h-edge" 
                                className='l-flex--row-nowrap l-flex--center-center'
                                data-extension={extension}
                            >
                                { row === -1 ? String.fromCharCode(97+col) : null }
                            </div>
                        );
                    }
                }
            } else {
                let extension;
                if (row === 0) {
                    extension = "top";
                } else if (row === this.props.numberOfRowTile-1) {
                    extension = "bottom";
                } else {
                    extension = "none";
                }
                rowTile.push(
                    <div 
                        id="v-edge" 
                        className='l-flex--row-nowrap l-flex--center-center'
                        data-extension = {extension}
                    >
                        { row + 1 }
                    </div>
                )
                let rowBoard = []
                for(let col=0; col < this.props.numberOfRowTile; col++) {
                    rowBoard.push(
                        <Tile
                            setDisk={() => this.flip(row, col)} 
                            isWhite={this.state.diskColors[row][col]}
                            suggested={this.state.suggestedTiles[row][col]}
                        />
                    );
                }
                let position = "none";
                if (row === 0) {
                    position = "top";
                } else if (row === this.props.numberOfRowTile-1) {
                    position = "bottom"
                }
                rowTile.push(
                    <div 
                        id="board-row"
                        className='l-flex--row-nowrap l-flex--center-center'
                        data-l-position={position}
                    >
                        {rowBoard}
                    </div>
                );
                rowTile.push(
                    <div 
                        id="v-edge"
                        data-extension = {extension}
                    >
                    </div>
                )
            }
            displayTile.push(
                <div 
                    className='l-flex--row-nowrap l-flex--center-center'
                >
                    {rowTile}
                </div>
            );
        }
        return displayTile;
    }
    
    render() {
        return (
            <div id="outer" class='h-center'>
                { this.display() }
            </div>
        )
    }
}

export default Board;