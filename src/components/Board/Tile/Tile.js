import Disk from '../../Disk/Disk';
import React, {Component} from 'react';
import './tile.css';

class Tile extends Component {
    constructor(props) {
        super(props);
        this.setDisk = this.setDisk.bind(this);

        this.state = {
            isWhite: this.props.isWhite,
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            this.setState({
                isWhite: nextProps.isWhite,
            });
        }
    }

    display() {
        if (this.props.isWhite === undefined) {
            return undefined;
        } else {
            return <Disk isWhite={this.state.isWhite} />;
        }
    }

    setDisk() {
        this.props.setDisk();
    }

    render() {
        return (
            <div 
                id="tile" 
                className='l-flex--row-nowrap l-flex--center-center'
                onClick={() => this.setDisk()}
                data-suggested={this.props.suggested} 
                >
                {this.display()}
            </div>
        )
    }
}

export default Tile;