import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './turn.css';

class Turn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            aiTurn: this.props.aiTurn,
            secondPlayerName: this.props.secondPlayerName,
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            this.setState({
                aiTurn: nextProps.aiTurn,
                secondPlayerName: nextProps.secondPlayerName,
            });
        }
    }

    render() {
        return(
            <div id="c-turn">
                <div id="l-turn__ai" className="l-turn__name h-font--bold h-animation-config" data-active={this.state.aiTurn}><p>{this.state.secondPlayerName}</p></div>
                <div id="l-turn__human" className="l-turn__name h-font--bold h-animation-config" data-active={!this.state.aiTurn}><p>YOUR</p></div>
                <div id="l-turn__text" className='h-font--small h-animation-config' data-aiturn={this.state.aiTurn}><p>TURN</p></div>
            </div>
        );
    }
}

Turn.propTypes = {
    aiTurn: PropTypes.bool,
};

export default Turn;